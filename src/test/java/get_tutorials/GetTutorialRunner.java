package get_tutorials;

import com.intuit.karate.junit5.Karate;

public class GetTutorialRunner {
    @Karate.Test
    Karate testUser(){
        return Karate.run("classpath:get_tutorials/get_tutorial.feature");
    }


}
