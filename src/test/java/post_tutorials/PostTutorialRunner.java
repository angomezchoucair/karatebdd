package post_tutorials;

import com.intuit.karate.junit5.Karate;

public class PostTutorialRunner {
    @Karate.Test
    Karate testUser(){
        return Karate.run("classpath:post_tutorials/post_tutorial.feature");
    }
}
